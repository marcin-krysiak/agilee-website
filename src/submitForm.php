<?php
use PHPMailer\PHPMailer\PHPMailer;
require 'PHPMailer.php';

$mail = new PHPMailer();
$name = $_POST["name"];
$email = $_POST["email"];
$message = $_POST["message"];
$question = $_POST["question"];
$to = "contact@agilee.io";
$subject = "New message from Agilee IO website from $name";
$headers = "From: $email\n";
if (empty($question)) {
  foreach ($_FILES["file"]["name"] as $i => $fileName) {
    if (!empty($fileName)) {
      $mail->AddAttachment(
        $_FILES["file"]["tmp_name"][$i],
        $_FILES["file"]["name"][$i]
      );
    }
  }
  $mail->SetFrom($email, $name);
  $mail->addAddress($to, 'Agilee IO Limited - Marcin Krysiak');
  $mail->Subject = $subject;
  $mail->Body = $message;
  if ($mail->send()) {
    echo "email sent successful";
  } else {
    die("Opps..., something went wrong. Please try again later");
  }
} else {
  die("Opps..., something went wrong. Please try again later");
}
