import { Component, h, Host } from "@stencil/core";
import Parallax from "scroll-parallax";
import preset from "jss-preset-default";
import jss from "jss";
import globalStyles from "../../theme/global";
import styles from "./styles";
import { Build } from "@stencil/core";

// do once in entry js
jss.setup(preset());
jss.createStyleSheet(globalStyles).attach();

const { classes } = jss.createStyleSheet(styles).attach();

@Component({
  tag: "app-home",
})
export class AppHome {
  componentDidLoad() {
    if (Build.isBrowser) {
      new Parallax(".parallax", {
        intensity: 50,
      }).init();
    }
  }
  render() {
    return (
      <Host class={classes.content}>
        <app-nav />
        <app-hero />
        <app-services />
        <app-portfolio />
        <app-clients />
        <app-contact />
      </Host>
    );
  }
}
