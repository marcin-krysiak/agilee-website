import { Component, Host, h, Prop } from "@stencil/core";
import jss from "jss";
import styles from "./styles";
import preset from "jss-preset-default";
import { Build } from "@stencil/core";

jss.setup(preset());
const { classes } = jss.createStyleSheet(styles).attach();

@Component({
  tag: "app-hero",
})
export class Hero {
  @Prop() image: string;

  private heroContent!: HTMLDivElement;

  componentDidLoad() {
    if (Build.isBrowser) {
      this.heroContent &&
        document.addEventListener("scroll", () => {
          const offset = window.pageYOffset;
          const newOpacity = 1 - (offset / (window.innerHeight * 0.85)) * 2.5;
          if (newOpacity > 0) {
            this.heroContent.style.opacity = (newOpacity > 0
              ? newOpacity
              : 0
            ).toString();
          }
        });
    }
  }

  render() {
    return (
      <Host class={classes.container}>
        <video loop autoplay muted class={classes.video + " parallax"}>
          <source src="./assets/video/videoplayback.mp4" type="video/mp4" />
        </video>
        <div class={classes.colorMask} />
        <div class={classes.heroContent} ref={el => (this.heroContent = el)}>
          <div class={classes.logo}>
            <app-logo-link />
          </div>
          <h1 class={classes.header}>
            We deliver bespoke web and mobile applications
          </h1>
          <app-contact-link class={classes.contactUs} />
        </div>
      </Host>
    );
  }
}
