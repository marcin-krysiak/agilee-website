import colors from "../../theme/colors";
import { Styles } from "jss";
import animations from "../../theme/animations";

const lightSpeedDelay = 4;

export default <Styles | any>{
  ...animations.lightSpeed,
  container: {
    display: "flex",
    justifyContent: "center",
    position: "relative",
    width: "100%",
    minHeight: "165px",
    backgroundColor: colors.black,
    padding: "2rem",
  },
  content: {
    display: "flex",
    alignItems: "center",
    flexDirection: "column",
    width: "100%",
    maxWidth: 1600,
    color: colors.white,
  },
  logos: {
    display: "flex",
    flexWrap: "wrap",
    justifyContent: "space-evenly",
    width: "100%",
    margin: "20px 0",

    "& img": {
      position: "absolute",
      opacity: 0,
      animationFillMode: "forwards",
      animation: `$lightSpeed 30s ease-out infinite`,
      height: 50,

      "&:nth-of-type(1)": { animationDelay: "1s" },
      "&:nth-of-type(2)": { animationDelay: `${lightSpeedDelay}s` },
      "&:nth-of-type(3)": { animationDelay: `${lightSpeedDelay * 2}s` },
      "&:nth-of-type(4)": { animationDelay: `${lightSpeedDelay * 3}s` },
      "&:nth-of-type(5)": { animationDelay: `${lightSpeedDelay * 4}s` },
      "&:nth-of-type(6)": { animationDelay: `${lightSpeedDelay * 5}s` },
      "&:nth-of-type(7)": { animationDelay: `${lightSpeedDelay * 6}s` },
      "&:nth-of-type(8)": { animationDelay: `${lightSpeedDelay * 7}s` },
    },
  },
};
