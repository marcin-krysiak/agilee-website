import colors from "../../theme/colors";
import { Styles } from "jss";

export default <Styles>{
  container: {
    position: "fixed",
    display: "flex",
    justifyContent: "center",
    backgroundColor: colors.primary,
    width: "100%",
    height: 80,
    zIndex: 100,
    opacity: 0.8,
  },
  content: {
    position: "absolute",
    width: "100%",
    maxWidth: 1600,
  },
  logo: {
    position: "absolute",
    top: 20,
    left: -8,
    width: 150,
    height: 25,
    opacity: 1,
    transform: "scale(0.5)",
  },
  contactUs: {
    position: "absolute",
    textAlign: "right",
    top: 24,
    right: 20,
    opacity: 1,
    boxSizing: "border-box",
  },
};
