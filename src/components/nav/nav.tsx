import { Component, h, Host, Prop } from "@stencil/core";
import jss from "jss";
import styles from "./styles";
import preset from "jss-preset-default";
import { Build } from "@stencil/core";

jss.setup(preset());
const { classes } = jss.createStyleSheet(styles).attach();

@Component({
  tag: "app-nav",
})
export class AppNav {
  private navElement!: HTMLElement;
  @Prop() image: string;

  setOpacity() {
    if (Build.isBrowser) {
      const offset = window.pageYOffset;
      const newOpacity = offset / (window.innerHeight * 0.85) / 0.75;
      if (newOpacity < 0.9) {
        this.navElement.style.opacity = (newOpacity <= 0.9
          ? newOpacity
          : 0.9
        ).toString();
      }
    }
  }

  componentDidLoad() {
    if (this.navElement) {
      this.setOpacity();
      document.addEventListener("scroll", () => {
        this.setOpacity();
      });
    }
  }

  render() {
    return (
      <Host class={classes.container} ref={el => (this.navElement = el)}>
        <div class={classes.content}>
          <app-logo-link class={classes.logo} />
          <app-contact-link class={classes.contactUs} />
        </div>
      </Host>
    );
  }
}
