import { Component, h } from "@stencil/core";
import jss from "jss";
import styles from "./styles";
import preset from "jss-preset-default";

jss.setup(preset());
const { classes } = jss.createStyleSheet(styles).attach();

@Component({
  tag: "app-contact-link",
})
export class ContactLink {
  render() {
    return (
      <a href="#contact-us" class={classes.link}>
        <img src="assets/icons/communication.svg" alt="contact-us icon" />
        Say hello
      </a>
    );
  }
}
