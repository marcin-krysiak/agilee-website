import { Component, Host, h } from "@stencil/core";
import jss from "jss";
import styles from "./styles";
import { MDCTextField } from "@material/textfield";
import { MDCRipple } from "@material/ripple";
import preset from "jss-preset-default";
import { Build } from "@stencil/core";

jss.setup(preset());
const { classes } = jss.createStyleSheet(styles).attach();

@Component({
  tag: "app-contact",
  styleUrl: "material-ui.scss",
})
export class Contact {
  private cardBackEl!: HTMLDivElement;

  componentDidLoad() {
    if (Build.isBrowser) {
      document
        .querySelectorAll(".mdc-text-field")
        .forEach(element => new MDCTextField(element));

      new MDCRipple(document.querySelector(".mdc-button"));
    }
  }

  onSubmit = event => {
    event.preventDefault();
    const formData = new FormData(event.target);

    if (formData.get("question") !== "") return false;
    event.target.parentElement.classList.add("rotate");

    fetch(event.target.action, {
      method: event.target.method,
      body: formData,
    })
      .then(response => {
        if (response && response.ok) return "Message sent successfully";
        throw new Error(
          "Oppsss... Something went wrong. Please try again later"
        );
      })
      .then(message => (this.cardBackEl.innerHTML = `<h2>${message}</h2>`))
      .catch(err => (this.cardBackEl.innerHTML = `<h2>${err.message}</h2>`));
  };

  render() {
    return (
      <Host class={classes.container}>
        <h1 id="contact-us">Say hello</h1>
        <p>Tell us about your project ideas or just say hello</p>
        <div class={`mdc-card ${classes.card}`}>
          <form
            class={classes.form}
            method="post"
            onSubmit={this.onSubmit}
            action="submitForm.php"
          >
            <div
              class={`mdc-text-field mdc-text-field--outlined ${classes.textField}`}
            >
              <input
                type="text"
                id="name"
                name="name"
                required
                class="mdc-text-field__input"
                value=""
              />
              <div class="mdc-notched-outline">
                <div class="mdc-notched-outline__leading" />
                <div class="mdc-notched-outline__notch">
                  <label class="mdc-floating-label" htmlFor="name">
                    Name
                  </label>
                </div>
                <div class="mdc-notched-outline__trailing" />
              </div>
            </div>

            <div
              class={`mdc-text-field mdc-text-field--outlined ${classes.textField}`}
            >
              <input
                type="text"
                id="email"
                name="email"
                required
                class="mdc-text-field__input"
                value=""
              />
              <div class="mdc-notched-outline">
                <div class="mdc-notched-outline__leading" />
                <div class="mdc-notched-outline__notch">
                  <label class="mdc-floating-label" htmlFor="email">
                    Email
                  </label>
                </div>
                <div class="mdc-notched-outline__trailing" />
              </div>
            </div>
            <div class="mdc-text-field-helper-line">
              <div class="mdc-text-field-helper-text">
                We will never share your email with anyone else
              </div>
            </div>
            <input
              type="hidden"
              id="question"
              name="question"
              class="mdc-text-field__input"
              value=""
            />

            <div
              class={`mdc-text-field mdc-text-field--textarea ${classes.textField}`}
            >
              <textarea
                id="message"
                name="message"
                required
                class="mdc-text-field__input"
                rows={8}
              />
              <div class="mdc-notched-outline">
                <div class="mdc-notched-outline__leading" />
                <div class="mdc-notched-outline__notch">
                  <label htmlFor="message" class="mdc-floating-label">
                    Message
                  </label>
                </div>
                <div class="mdc-notched-outline__trailing" />
              </div>
            </div>

            <label htmlFor="file" class="mdc-button mdc-button--outlined">
              <div class="mdc-button__ripple" />
              <span class="mdc-button__label">Attach Files</span>
            </label>
            <input
              class={classes.fileInput}
              id="file"
              name="file[]"
              multiple
              type="file"
              value=""
            />

            <button
              class={`mdc-button mdc-button--unelevated ${classes.button}`}
              type="submit"
            >
              <div class="mdc-button__ripple" />
              <span class="mdc-button__label">Send</span>
            </button>
          </form>
          <div class={classes.cardBack} ref={el => (this.cardBackEl = el)}>
            <img
              src={"assets/logo/apple-icon-180x180.png"}
              alt="Agilee IO logo"
            />
          </div>
        </div>
      </Host>
    );
  }
}
