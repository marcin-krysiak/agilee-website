import { Component, Host, h } from "@stencil/core";
import jss from "jss";
import styles from "./styles";
import preset from "jss-preset-default";

jss.setup(preset());
const { classes } = jss.createStyleSheet(styles).attach();

@Component({
  tag: "app-services",
})
export class Services {
  render() {
    return (
      <Host class={classes.container}>
        <div class={classes.content}>
          <h1>How do we work</h1>

          <div class={classes.services}>
            <div class={classes.service}>
              <img
                src="assets/icons/services/devices.svg"
                alt="cross-platform icon"
                class={classes.serviceImage}
              />
              <h2>Cross-platform</h2>
              <p>
                We work smarter and cost-effective. Invest once in a single team
                and get a fully reusable, multiple platform code. Our PWA or
                native applications works on web and mobile.
              </p>
            </div>

            <div class={classes.service}>
              <img
                src="assets/icons/services/creative.svg"
                alt="creative icon"
                class={classes.serviceImage}
              />
              <h2>Creative</h2>
              <p>
                Early adoption of cutting edge technology let us be at the head
                when it comes to innovation. We helped start-ups to pioneer new
                areas and we even decided to start some of our own.
              </p>
            </div>

            <div class={classes.service}>
              <img
                src="assets/icons/services/reliability.svg"
                alt="reliability icon"
                class={classes.serviceImage}
              />
              <h2>Reliability</h2>
              <p>
                We always work with modern, dependency free and well tested
                code. That allows us to enhance the performance and reduces the
                learning curve in case of hand-over.
              </p>
            </div>
          </div>
          <div class={classes.technologies}>
            <img src="assets/icons/technologies/ionic.svg" alt="ionic icon" />
            <img
              src="assets/icons/technologies/angular.svg"
              alt="angular icon"
            />
            <img src="assets/icons/technologies/aws.svg" alt="aws icon" />
            <img src="assets/icons/technologies/node.svg" alt="node icon" />
            <img src="assets/icons/technologies/react.svg" alt="react icon" />
            <img src="assets/icons/technologies/vue.svg" alt="vue icon" />
          </div>
        </div>
      </Host>
    );
  }
}
