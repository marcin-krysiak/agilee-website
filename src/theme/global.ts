// import paperCard from './components/paper-card';
// import paperButton from './components/paper-button';
import fonts from "./fonts";
import typography from "./typography";
import spaces from "./spaces";

import textField from "./components/text-field";

export default <any>{
  "@global": {
    ...fonts,
    ...typography,
    ...spaces,

    ...textField,

    html: {
      scrollBehavior: "smooth",
    },
    a: {
      cursor: "pointer",
      textDecoration: "none",
      color: "unset",
    },
  },
};
