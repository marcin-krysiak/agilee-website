export default {
  "@font-face": [
    {
      fontFamily: "comfortaa-light",
      src: "url(assets/fonts/Comfortaa-Light.ttf)",
    },
    {
      fontFamily: "comfortaa-bold",
      src: "url(assets/fonts/Comfortaa-Bold.ttf)",
    },
    {
      fontFamily: "raleway-light",
      src: "url(assets/fonts/Raleway-Light.ttf)",
    },
    {
      fontFamily: "raleway-bold",
      src: "url(assets/fonts/Raleway-Bold.ttf)",
    },
  ],
};
