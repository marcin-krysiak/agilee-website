export default {
  primary: "#0fd1be",
  black: "#383330",
  accent1: "#dbf6f7",
  accent2: "#ebe2c8",
  white: "#fff",
};
