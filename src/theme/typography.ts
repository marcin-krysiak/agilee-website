export default {
  body: {
    fontFamily: "raleway-light",
  },

  "h1, h2, h3, h4, h5, h6": {
    fontFamily: "comfortaa-light",
    fontWeight: "normal",
    lineHeight: "2rem",
  },
  "span, div, p": {
    fontFamily: "raleway-light",
    lineHeight: "1.5rem",
  },
};
