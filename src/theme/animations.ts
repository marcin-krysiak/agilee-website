export default {
  fadeIn: {
    "@keyframes fadeIn": {
      from: { opacity: 0 },
      to: { opacity: 1 },
    },
  },
  bounce: {
    "@keyframes bounce": {
      "0%": { transform: "scale(1,1)      translateY(0)" },
      "10%": { transform: "scale(1.1,.9)   translateY(0)" },
      "30%": { transform: "scale(.9,1.1)   translateY(-14px)" },
      "50%": { transform: "scale(1.05,.95) translateY(0)" },
      "57%": { transform: "scale(1,1)      translateY(-7px)" },
      "64%": { transform: "scale(1,1)      translateY(0)" },
      "100%": { transform: "scale(1,1)      translateY(0)" },
    },
  },
  spinner: {
    "@keyframes spinner": {
      from: {
        transform: "rotate(0deg)",
      },
      to: {
        transform: "rotate(360deg)",
      },
    },
  },
  lightSpeed: {
    "@keyframes lightSpeed": {
      "0%": {
        transform: "translate3d(300px, 0, 0) skewX(-30deg)",
        opacity: 0,
      },
      "1.5%": {
        transform: "skewX(20deg)",
        opacity: 1,
      },
      "2.0%": {
        transform: "skewX(-5deg)",
      },
      "5.0%": {
        transform: "translate3d(0, 0, 0)",
      },
      "7.0%": {
        transform: "skewX(5deg)",
      },
      "8.5%": {
        transform: "skewX(-20deg)",
        opacity: 1,
      },
      "10.0%": {
        transform: "translate3d(-300px, 0, 0) skewX(30deg)",
        opacity: 0,
      },
    },
  },
};
